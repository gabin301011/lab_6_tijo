package pl.edu.pwsztar.domain.mapper;
@FunctionalInterface
public interface InterfaceConvert<F, T> {
        T convert(F from);
}
