package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.MovieCounterDto;

@Component
public class MovieCounterMapper implements InterfaceConvert<Long, MovieCounterDto> {

    @Override
    public MovieCounterDto convert(Long from) {
        return new MovieCounterDto(from);
    }
}
